#include <utility>

#ifndef BOTIA_FILELOGGER_H
#define BOTIA_FILELOGGER_H

#include "logger.h"
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <boost/filesystem.hpp>

namespace Botia
{
    class FileLogger : public Logger
    {
    public:
        explicit FileLogger(std::string fileName, std::string path)
        {
            this->fileName = fileName;
            this->path = path;

            OpenFile();
        }

        void error(std::string message) override;

        void info(std::string message) override;

        void log(LogLevel level, std::string message) override;

    private:
        std::ofstream fileStream;
        boost::filesystem::path path;
        std::string fileName;

        void OpenFile()
        {
            if (!boost::filesystem::exists(this->path))
            {
                boost::filesystem::create_directory(this->path);
            }

            this->fileStream = std::ofstream(this->path.string() + "/" + this->fileName);
        };
    };
}

#endif //BOTIA_FILELOGGER_H
