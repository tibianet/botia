#ifndef BOTIA_DEFLATE_H
#define BOTIA_DEFLATE_H

#include "compression.h"
#include <string.h>
#include <zlib.h>
#include <exception>
#include <string>

namespace Botia
{
    class InflateInitFailedException : public std::exception
    {
    };

    class DeflateInitFailedException : public std::exception
    {
    };

    class Deflate : public Compression
    {
    public:
        explicit Deflate(int windowBits)
        {
            this->windowBits = windowBits;
        };

        void Decompress(char *inputBuffer, int inputSize, char *outputBuffer, int outputSize) override;

        void Compress(char *inputBuffer, int inputSize, char *outputBuffer, int outputSize) override;

        bool IsCompressed(char *inputBuffer) override;

    protected:
    private:
        int windowBits;
    };
};

#endif //BOTIA_DEFLATE_H
