#include <yaml-cpp/yaml.h>
#include <signal.h>

#include "src/socket.h"
#include "src/logger/filelogger.h"
#include "src/server/server.h"
#include "src/client/client.h"

#define MAIN_RETURN_ERROR -1
#define MAIN_RETURN_SUCCESS 0

#define DATA_DIRECTORY_PATH "./data"

Botia::Server *server;

void signalHandler(int signal)
{
    server->Stop();
}

int main()
{
    YAML::Node configuration;

    try
    {
        configuration = YAML::LoadFile(std::string(DATA_DIRECTORY_PATH) + "/config/configuration.yaml");
    }
    catch (YAML::BadFile &e)
    {
        printf("[-] Configuration cannot be loaded\n");
        return MAIN_RETURN_ERROR;
    }
    catch (YAML::ParserException &e)
    {
        printf("[-] Configuration is malformed\n");
        return MAIN_RETURN_ERROR;
    }

    auto serverListeningPort = configuration["server"]["listeningPort"].as<int>();

    if (!serverListeningPort)
    {
        printf("[-] Cannot load server listening port\n");
        return MAIN_RETURN_ERROR;
    }

    auto logger = new Botia::FileLogger("system.log", std::string(DATA_DIRECTORY_PATH) + "/logs");
    server = new Botia::Server(serverListeningPort);
    server->SetLogger(logger);
    server->Run();

    // Register signal handler
    struct sigaction sigIntHandler{};

    sigIntHandler.sa_handler = signalHandler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, nullptr);

#ifndef BOTIA_NON_BLOCKING_SERVER
    pause();
#endif

    return MAIN_RETURN_SUCCESS;
}