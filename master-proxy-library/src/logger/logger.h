#ifndef BOTIA_LOGGER_H
#define BOTIA_LOGGER_H

#include <string>
#include <map>

namespace Botia
{
    class Logger
    {
    public:
        enum LogLevel
        {
            INFO = 0,
            ERROR = 1,
        };

        virtual ~Logger() = default;

        virtual void error(std::string message) = 0;

        virtual void info(std::string message) = 0;

        virtual void log(LogLevel level, std::string message) = 0;

    protected:
        std::map<Botia::Logger::LogLevel, std::string> levelMap{
                {Botia::Logger::LogLevel::INFO,  "INFO"},
                {Botia::Logger::LogLevel::ERROR, "ERROR"}
        };
    };
}

#endif //BOTIA_LOGGER_H
