#include <gtest/gtest.h>

#include "src/compression/deflate.h"

TEST(Compression, Compression_DeflateBasic_Test)
{
    std::string uncompressedMessage("Example uncompressed message!");
    char compressedMessage[4096];

    auto deflateCompression = new Botia::Deflate(15);
    deflateCompression->Compress(uncompressedMessage.data(), static_cast<int>(uncompressedMessage.size()),
                                 compressedMessage, sizeof(compressedMessage));

    ASSERT_TRUE(deflateCompression->IsCompressed(compressedMessage));

    char decompressedMessage[4096];
    deflateCompression->Decompress(compressedMessage, sizeof(compressedMessage), decompressedMessage, sizeof(decompressedMessage));

    ASSERT_EQ(uncompressedMessage, std::string(decompressedMessage));
}