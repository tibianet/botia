#ifndef BOTIA_SERVER_H
#define BOTIA_SERVER_H

#include "../socket.h"
#include "../logger/logger.h"
#include "../compression/deflate.h"

#include "event/event.h"
#include "event/onconnectevent.h"
#include "event/ondisconnectevent.h"
#include "event/onreceiveevent.h"

#include "listener/listener.h"

#include <string>
#include <thread>
#include <vector>
#include <typeinfo>
#include <netinet/in.h>
#include <boost/format.hpp>

#ifdef __linux__
#include <cstring>
#endif

#define BOTIA_SERVER_BUFFER_SIZE 4096

namespace Botia
{
    class InvalidSocketException : public std::exception
    {
    };

    class Server
    {
    public:
        explicit Server(int port)
        {
            this->port = port;
            this->isWorking = false;
            this->serverSocket = INVALID_SOCKET;
        };

        ~Server()
        {
            Stop();
        };

        void Run();

        void SetLogger(Logger *logger)
        {
            this->logger = logger;
        };

        virtual void Stop()
        {
            // This will stop the server on next accept iteration with gratefully thread termination
            this->isWorking = false;

            close(this->serverSocket);

#ifdef BOTIA_NON_BLOCKING_SERVER
            if (acceptThread.joinable())
                acceptThread.join();
#endif
        };

        void AddListener(Botia::BaseServer::EvenType eventType, Botia::BaseServer::Listener *listener)
        {
            listeners.emplace(std::make_pair(eventType, listener));
        };

    protected:
        void HandleConnection(int inputSocket, struct sockaddr_in inputAddress);

        void OnConnect(int inputSocket, struct sockaddr_in inputAddress);

        void OnDisconnect(int inputSocket, struct sockaddr_in inputAddress);

        void OnReceive(int inputSocket, struct sockaddr_in inputAddress, char *receiveBuffer);

        bool Send(int inputSocket, char *buffer);

    private:
        void CreateSocket();

        void Accept();

        void NotifyListeners(Botia::BaseServer::EvenType eventType, Botia::BaseServer::Event &event)
        {
            std::map<Botia::BaseServer::EvenType, Botia::BaseServer::Listener *>::iterator iterator;
            for (iterator = listeners.begin(); iterator != listeners.end(); ++iterator)
            {
                auto itEventType = iterator->first;
                auto listener = iterator->second;

                if (itEventType == eventType)
                {
                    listener->Notify(&event);
                }
            }
        };

        int port;
        int serverSocket;
        bool isWorking;
        struct sockaddr_in serverAddress{};

        std::map<Botia::BaseServer::EvenType, Botia::BaseServer::Listener *> listeners;
        std::vector<std::thread> threads;
        std::thread acceptThread;

        Logger *logger{};
    };
}

#endif //BOTIA_SERVER_H
