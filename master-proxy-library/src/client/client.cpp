#include "client.h"

void Botia::Client::Connect()
{
    if (this->connectionSocket != INVALID_SOCKET)
    {
        return;
    }

    this->connectionSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (inet_addr(this->host.c_str()) == INVALID_SOCKET)
    {
        struct hostent *hostEnt;
        struct in_addr **inAddressList;

        hostEnt = gethostbyname(this->host.c_str());

        if (hostEnt == nullptr)
        {
            throw HostResolveException();
        }

        inAddressList = (struct in_addr **) hostEnt->h_addr_list;
        this->serverAddress.sin_addr = *inAddressList[0];
    }
    else
    {
        this->serverAddress.sin_addr.s_addr = inet_addr(this->host.c_str());
    }

    this->serverAddress.sin_family = AF_INET;
    this->serverAddress.sin_port = htons(this->port);

    if (connect(this->connectionSocket, (struct sockaddr *) &this->serverAddress, sizeof(this->serverAddress)) ==
        ERROR_OCCURRED)
    {
        throw ConnectionFailedException();
    }
}

bool Botia::Client::Send(char *buffer)
{
    if (this->connectionSocket == INVALID_SOCKET)
    {
        return false;
    }

    auto result = send(this->connectionSocket, buffer, strlen(buffer), 0);
    return result != ERROR_OCCURRED;
}

void Botia::Client::Receive(char *buffer, size_t bufferSize)
{
    auto result = recv(this->connectionSocket, buffer, bufferSize, 0);

    if (result == ERROR_OCCURRED)
    {
        throw ReceivingFailedException();
    }
}

void Botia::Client::Stop()
{
    close(this->connectionSocket);
}
