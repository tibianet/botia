# Botia - Master Proxy

Requirements:

* Boost Library (Latest Version)
* Yaml CPP (https://github.com/jbeder/yaml-cpp)

Supported OS:

* Linux/Unix - Tested on ubuntu *18.04*
* MacOS - Tested on *10.13.6*