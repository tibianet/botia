#include "deflate.h"

void Botia::Deflate::Decompress(char *inputBuffer, int inputSize, char *outputBuffer, int outputSize)
{
    z_stream decompressionStream;
    memset(&decompressionStream, 0, sizeof(decompressionStream));

    if (inflateInit2(&decompressionStream, this->windowBits) != Z_OK)
    {
        throw InflateInitFailedException();
    }

    decompressionStream.next_in = reinterpret_cast<Bytef *>(inputBuffer);
    decompressionStream.avail_in = static_cast<uint>(inputSize);

    char decompressedBuffer[outputSize];
    memset(&decompressedBuffer, 0, sizeof(decompressedBuffer));

    decompressionStream.next_out = reinterpret_cast<Bytef *>(decompressedBuffer);
    decompressionStream.avail_out = sizeof(decompressedBuffer);

    auto result = inflate(&decompressionStream, Z_NO_FLUSH);

    inflateEnd(&decompressionStream);

    if (result != Z_STREAM_END)
    {
        throw DecompressionFailedException();
    }

    memcpy(outputBuffer, decompressedBuffer, static_cast<size_t>(outputSize));
}

void Botia::Deflate::Compress(char *inputBuffer, int inputSize, char *outputBuffer, int outputSize)
{
    z_stream compressionStream;
    memset(&compressionStream, 0, sizeof(compressionStream));

    compressionStream.zalloc = Z_NULL;
    compressionStream.zfree = Z_NULL;
    compressionStream.opaque = Z_NULL;

    if (deflateInit2(&compressionStream, Z_DEFAULT_COMPRESSION, Z_DEFLATED, this->windowBits, 1, Z_DEFAULT_STRATEGY) !=
        Z_OK)
    {
        throw DeflateInitFailedException();
    }

    char compressedBuffer[outputSize];
    memset(&compressedBuffer, 0, sizeof(compressedBuffer));

    compressionStream.next_in = reinterpret_cast<Bytef *>(inputBuffer);
    compressionStream.avail_in = static_cast<uint>(inputSize);
    compressionStream.next_out = reinterpret_cast<Bytef *>(compressedBuffer);
    compressionStream.avail_out = sizeof(compressedBuffer);

    auto result = deflate(&compressionStream, Z_FINISH);

    deflateEnd(&compressionStream);

    if (result != Z_STREAM_END)
    {
        throw CompressionFailedException();
    }

    memcpy(outputBuffer, compressedBuffer, static_cast<size_t>(outputSize));
}

bool Botia::Deflate::IsCompressed(char *inputBuffer)
{
    uint32_t compressionHeader;
    std::copy(inputBuffer, inputBuffer + sizeof(uint32_t), reinterpret_cast<char *>(&compressionHeader));

    return static_cast<bool>((compressionHeader & 1 << 31) != 0);
}
