#ifndef BOTIA_ONRECEIVEEVENT_H
#define BOTIA_ONRECEIVEEVENT_H

#include "../../socket.h"
#include "event.h"

namespace Botia
{
    namespace BaseServer
    {
        class OnReceiveEvent : public Botia::BaseServer::Event
        {
        public:
            OnReceiveEvent(int inputSocket, const sockaddr_in &inputAddress, char *receiveBuffer)
            {
                this->inputSocket = inputSocket;
                this->inputAddress = inputAddress;
                this->receiveBuffer = receiveBuffer;
            }

        protected:
            int inputSocket;
            struct sockaddr_in inputAddress{};
            char *receiveBuffer;
        };
    };
};

#endif //BOTIA_ONRECEIVEEVENT_H
