#include "filelogger.h"

void Botia::FileLogger::error(std::string message)
{
    this->log(LogLevel::ERROR, message);
}

void Botia::FileLogger::info(std::string message)
{
    this->log(LogLevel::INFO, message);
}

void Botia::FileLogger::log(LogLevel level, std::string message)
{
    auto now = std::chrono::system_clock::now();
    auto dateTime = std::chrono::system_clock::to_time_t(now);
    auto describedLevel = this->levelMap.find(level)->second;

    this->fileStream << "[ " << std::put_time(std::localtime(&dateTime), "%F %T") << " ]" << " : " << "[ "
                     << describedLevel << " ] " << message << std::endl;
    this->fileStream.flush();
}
