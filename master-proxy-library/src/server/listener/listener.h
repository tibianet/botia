#ifndef BOTIA_LISTENER_H
#define BOTIA_LISTENER_H

#include "../event/event.h"

namespace Botia
{
    namespace BaseServer
    {
        class Listener
        {
        public:
            virtual void Notify(Event *event) = 0;

        protected:
            Listener() = default;

        private:
            Listener(const Listener &);

            Listener &operator=(Listener &);
        };
    }
}

#endif //BOTIA_LISTENER_H
