#ifndef BOTIA_EVENT_H
#define BOTIA_EVENT_H

namespace Botia
{
    namespace BaseServer
    {
        enum EvenType
        {
            OnConnect,
            OnDisconnect,
            OnReceive
        };

        class Event
        {
        protected:
            Event &operator=(const Event &that) = default;

            Event() = default;

            Event(const Event &that) = default;

            virtual ~Event() = default;
        };
    }
};

#endif //BOTIA_EVENT_H
