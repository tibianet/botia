#ifndef BOTIA_ONCONNECTEVENT_H
#define BOTIA_ONCONNECTEVENT_H

#include "../../socket.h"
#include "event.h"

namespace Botia
{
    namespace BaseServer
    {
        class OnConnectEvent : public Botia::BaseServer::Event
        {
        public:
            explicit OnConnectEvent(int inputSocket, const sockaddr_in &inputAddress)
            {
                this->inputSocket = inputSocket;
                this->inputAddress = inputAddress;
            }

            int inputSocket;
            struct sockaddr_in inputAddress{};
        };
    };
};

#endif //BOTIA_ONCONNECTEVENT_H
