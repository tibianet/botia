#ifndef BOTIA_ONDISCONNECTEVENT_H
#define BOTIA_ONDISCONNECTEVENT_H

#include "../../socket.h"
#include "event.h"

namespace Botia
{
    namespace BaseServer
    {
        class OnDisconnectEvent : public Botia::BaseServer::Event
        {
        public:
            OnDisconnectEvent(int inputSocket, const sockaddr_in &inputAddress)
            {
                this->inputSocket = inputSocket;
                this->inputAddress = inputAddress;
            }

        protected:
            int inputSocket;
            struct sockaddr_in inputAddress{};
        };
    };
};

#endif //BOTIA_ONDISCONNECTEVENT_H
