#include <utility>

#ifndef BOTIA_CLIENT_H
#define BOTIA_CLIENT_H

#define ERROR_OCCURRED -1

#include "../socket.h"
#include "../logger/logger.h"
#include <string>
#include <thread>
#include <vector>
#include <netinet/in.h>
#include <boost/format.hpp>

#ifdef __linux__
#include <cstring>
#endif

namespace Botia
{
    class HostResolveException : public std::exception
    {
    };

    class ConnectionFailedException : public std::exception
    {
    };

    class ReceivingFailedException : public std::exception
    {
    };

    class Client
    {
    public:
        Client(std::string host, int port)
        {
            this->host = std::move(host);
            this->port = port;
            this->connectionSocket = INVALID_SOCKET;
        };

        ~Client()
        {
            Stop();
        };

        void Connect();

        bool Send(char *buffer);

        void Receive(char *buffer, size_t bufferSize);

        void Stop();

    private:
        int connectionSocket;
        struct sockaddr_in serverAddress{};
        std::string host;
        int port;
    };
};

#endif //BOTIA_CLIENT_H
