#include "../socket.h"
#include "server.h"

void Botia::Server::Run()
{
    try
    {
        CreateSocket();
    }
    catch (InvalidSocketException &exception)
    {
        this->logger->error("Cannot create server connectionSocket");
        return;
    }

    memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddress.sin_port = htons(port);

    printf("[+] Server is listening on port: %d\n", port);

    bind(serverSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress));
    listen(serverSocket, SOMAXCONN);

#ifdef BOTIA_NON_BLOCKING_SERVER
    std::thread acceptThread(&Server::Accept, this);
    this->acceptThread = std::move(acceptThread);
#else
    Accept();
#endif
}

void Botia::Server::CreateSocket()
{
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (serverSocket == INVALID_SOCKET)
    {
        throw InvalidSocketException();
    }
}

void Botia::Server::HandleConnection(int inputSocket, struct sockaddr_in inputAddress)
{
    auto clientIpAddress = inet_ntoa(inputAddress.sin_addr);
    printf("[+] Connected from %s\n", clientIpAddress);

    OnConnect(inputSocket, inputAddress);

    while (true)
    {
        char buffer[BOTIA_SERVER_BUFFER_SIZE];

        auto received = recv(inputSocket, buffer, BOTIA_SERVER_BUFFER_SIZE, 0);

        if (received == RECV_DISCONNECT)
        {
            OnDisconnect(inputSocket, inputAddress);
            break;
        }
        else if (received == RECV_ERROR_OCCURRED)
        {
            this->logger->error(boost::str(boost::format("Receiving error occurred on buffer: %1%") % received));
        }
        else
        {
            OnReceive(inputSocket, inputAddress, buffer);
        }

        // Reset received buffer
        memset(&buffer, 0, BOTIA_SERVER_BUFFER_SIZE);
    }

    close(inputSocket);
}

void Botia::Server::OnConnect(int inputSocket, struct sockaddr_in inputAddress)
{
    this->logger->info("Connection established from %s");
    printf("[+] Called event: OnConnect\n");

    auto event = new Botia::BaseServer::OnConnectEvent(inputSocket, inputAddress);
    this->NotifyListeners(Botia::BaseServer::EvenType::OnConnect, *event);
}

void Botia::Server::OnDisconnect(int inputSocket, struct sockaddr_in inputAddress)
{
    printf("[+] Called event: OnDisconnect\n");

    auto event = new Botia::BaseServer::OnDisconnectEvent(inputSocket, inputAddress);
    this->NotifyListeners(Botia::BaseServer::EvenType::OnDisconnect, *event);
}

void Botia::Server::OnReceive(int inputSocket, struct sockaddr_in inputAddress, char *receiveBuffer)
{
    printf("[+] Called event: OnReceive with data: %s\n", receiveBuffer);

    auto compression = new Botia::Deflate(15);
    if (compression->IsCompressed(receiveBuffer))
    {
        char temporaryCompressionBuffer[BOTIA_SERVER_BUFFER_SIZE];
        compression->Decompress(receiveBuffer, sizeof(receiveBuffer), temporaryCompressionBuffer,
                                sizeof(temporaryCompressionBuffer));

        *receiveBuffer = *temporaryCompressionBuffer;
    }

    auto event = new Botia::BaseServer::OnReceiveEvent(inputSocket, inputAddress, receiveBuffer);
    this->NotifyListeners(Botia::BaseServer::EvenType::OnReceive, *event);
}

void Botia::Server::Accept()
{
    printf("[+] Waiting for the first client...\n");

    this->isWorking = true;
    while (true)
    {
        if (!this->isWorking)
        {
            break;
        }

        struct sockaddr_in clientAddress{};
        socklen_t clientAddressSize = sizeof(clientAddress);

        auto clientSocket = accept(serverSocket, (struct sockaddr *) &clientAddress, &clientAddressSize);

        if (clientSocket == INVALID_SOCKET)
        {
            break;
        }

        threads.emplace_back(std::thread(&Server::HandleConnection, this, clientSocket, clientAddress));
    }

    for (auto &thread : threads)
    {
        if (thread.joinable())
            thread.join();
    }
}

bool Botia::Server::Send(int inputSocket, char *buffer)
{
    if (inputSocket == INVALID_SOCKET)
    {
        return false;
    }

    auto result = send(inputSocket, buffer, strlen(buffer), 0);
    return result != ERROR_OCCURRED;
}
