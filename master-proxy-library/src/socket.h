#ifndef BOTIA_SOCKET_H
#define BOTIA_SOCKET_H

#if !(defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__)))
#error "Only Unix is supported"
#endif

#define INVALID_SOCKET -1
#define RECV_ERROR_OCCURRED -1
#define RECV_DISCONNECT 0
#define ERROR_OCCURRED -1

#include <sys/types.h>     /*  required by sys/connectionSocket.h */
#include <sys/socket.h>    /*  sockets */
#include <sys/time.h>      /*  timeval */
#include <sys/ioctl.h>     /*  ioctl  */
#include <netinet/in.h>    /*  INADDR_*, in_addr, sockaddr_in, htonl etc. */
#include <netdb.h>         /*  getservbyname */
#include <arpa/inet.h>     /*  inet_addr */
#include <errno.h>         /*  connectionSocket error codes */
#include <unistd.h>

#endif //BOTIA_SOCKET_H
