#ifndef BOTIA_COMPRESSION_H
#define BOTIA_COMPRESSION_H

#include <exception>

namespace Botia
{
    class DecompressionFailedException : public std::exception
    {
    };

    class CompressionFailedException : public std::exception
    {
    };

    class InsufficientOutputBufferSizeException : public std::exception
    {
    };

    class Compression
    {
    public:
        virtual void Decompress(char *inputBuffer, int inputSize, char *outputBuffer, int outputSize) = 0;

        virtual void Compress(char *inputBuffer, int inputSize, char *outputBuffer, int outputSize) = 0;

        virtual bool IsCompressed(char *inputBuffer) = 0;
    };
};

#endif //BOTIA_COMPRESSION_H
